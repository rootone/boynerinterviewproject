﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using BoynerProject.Consumer.Consumer;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

namespace BoynerProject.Consumer
{
    public class Program
    {
        public static void Main(string[] args)
        {
            CreateWebHostBuilder(args).Build().Run();
        }

        public static IWebHostBuilder CreateWebHostBuilder(string[] args)
        {
            SetRedis setRedis = new SetRedis();
            setRedis.SetDb();
            //  GetMessage getMessages = new GetMessage();
            //  getMessages.Subscribe();
            return WebHost.CreateDefaultBuilder(args).UseStartup<Startup>();
        }
    }
}
