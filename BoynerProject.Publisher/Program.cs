﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using BoynerProject.Publisher.Publisher;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

namespace BoynerProject.Publisher
{
    public class Program
    {
        public static void Main(string[] args)
        {
            CreateWebHostBuilder(args).Build().Run();
        }

        public static IWebHostBuilder CreateWebHostBuilder(string[] args)
        {
            SendMessages sendMessages = new SendMessages();
            sendMessages.Publish();
            return WebHost.CreateDefaultBuilder(args).UseStartup<Startup>();
        }
    }
}
